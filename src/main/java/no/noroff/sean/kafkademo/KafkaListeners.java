package no.noroff.sean.kafkademo;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class KafkaListeners {
    @KafkaListener(topics = "YourTopicNameHere",
        groupId = "myGroupId"
    )
    public void listener(String data) {
        System.out.println(LocalDateTime.now() + "[Incoming event]: " + data);
    }
}
